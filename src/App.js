import React from 'react';
import { Route, Redirect, Switch} from 'react-router-dom';
import styles from './App.module.scss';
import Navigation from './components/Navigation';
import routerConfig from './routes';

const App = () => {
    const routes = routerConfig.routes.map(route => (
        <Route 
          key={route.id}
          path={route.path}
          component={route.component}
          exact={route.exact}
        />
    ));

    const redirects = routerConfig.redirects.map(redirect => (
        <Redirect 
          key={redirect.id}
          from={redirect.from}
          to={redirect.to}
          exact={redirect.exact}
        />
    ))

    return (
        <div className={styles.container}>
            <Navigation />
            <Switch>
                {routes}
                {redirects}
            </Switch>
        </div>
    );
}

export default App;