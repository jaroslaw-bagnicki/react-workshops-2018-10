import MoviesList from '../components/MoviesList';
import MovieDetail from '../components/MovieDetail';
import BookingsList from '../components/BookingsList';


export default {
  routes: [
    {
      id: 1,
      path: '/movies',
      component: MoviesList,
      exact: true
    },
    {
      id: 2,
      path: '/movies/:id',
      component: MovieDetail,
      exact: true
    },
    {
      id: 3,
      path: '/mybookings',
      component: BookingsList,
      exact: true
    },
  ],
  redirects: [
    {
      id: 1,
      from: '/',
      to: '/movies',
      exact: true
    },
    {
      id: 2,
      from: '/redirect',
      to: '/movies'
    }
  ]
}