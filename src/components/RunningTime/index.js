import React from 'react';
import styles from './RunningTime.module.scss';
import iconTimes from './icons/time_icon.svg';

const RunningTime = () => (
  <span className={styles.container}>
     <img src={iconTimes} alt="" />
     <span className={styles.duration}>Czas trwania: 2h 10min</span>
  </span>
)

export default RunningTime;