import React from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import RunningTime from '../RunningTime';
import Alert from '../Alert';
import Counter from '../Counter';
import Rating from '../Rating';
import Booking from '../Booking';

const Card = ({title, releaseDate, desc, duration, img, soldedOut, alert, rating, userRating, shows}) => {
  return (
    <div className={styles.container}>
      <div className={styles.movieDetails}>
          <div className={styles.cover}>
            <img className={styles.coverImg} src={img} alt="cover"/>
          </div>
          <div className={styles.content}>
            <h3 className={styles.title}>{title}</h3>
            <h5 className={styles.premiere}>Premiera: {releaseDate} r.</h5>
            <RunningTime {...duration}/>
            <p className={styles.synopsis}>
              {desc}
              </p>
          </div>
          <div className={styles.actions}>
            <Alert {...alert} fontSize='.9em' />
            <Counter />
            <Rating rating={rating} userRating={userRating} allowClear={true} />
          </div>
        </div>
        <Booking shows={shows} soldedOut={soldedOut} />
      </div>
     
  )
}

Card.propTypes = {
  title: PropTypes.string,
  releaseDate: PropTypes.string,
  desc: PropTypes.string,
  duration: PropTypes.number,
  img: PropTypes.string,
  soldedOut: PropTypes.bool,
}

export default Card;