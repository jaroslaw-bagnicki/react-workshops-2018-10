import React, {Component} from 'react';
import styles from './Rating.module.scss';
import Label from '../Label';
import icons from './icons.svg';
import classNames from 'classnames/bind';

const classList = classNames.bind(styles);

class Rating extends Component {
  constructor(props) {
    super();
    this.state = {
      rating: props.rating,
      userRating: props.userRating,
      allowClear: props.allowClear || false,
      currLabel: null
    };
  }

  static ratesDict = new Map([
    [0.5, 'Gorzej niż dno'],
    [1, 'Dno'],
    [1.5, 'Dno +'],
    [2, 'Ujdzie'],
    [2.5, 'Ujdzie +'],
    [3, 'Zjadliwy'],
    [3.5, 'Zjadliwy +'],
    [4, 'Dobry'],
    [4.5, 'Dobry +'],
    [5, 'Arcydzieło']
  ])

  createStars() {
    const roundedRating = Math.round(this.state.rating * 2) / 2;

    const starsParams = [...Rating.ratesDict.keys()].map(rate => ({
      rate,
      cssClassList: (rate <= roundedRating) ? ['star', 'full']: ['star', 'empty']
    }));

    const stars = starsParams.map(data => (
        <svg key={data.rate} data-rate={data.rate} height="1em" viewBox="0 0 288 512" 
          className={classNames(classList(data.cssClassList))}  
          onClick={() => this.placeRate(data.rate)}
          onMouseOver={() => this.setLabel(data.rate)}
          onMouseLeave={() => this.unsetLabel()}
        >
          {(data.rate % 1 !== 0) ? <use href={`${icons}#star`} />: <use href={`${icons}#star2`} />}
        </svg>
      )
    );
    return stars;
  } 

  placeRate(userRating) {
    if (!this.state.userRating) {
      this.setState({
         userRating,
         currLabel: 'Dzięki za ocenę :)'
       });
     setTimeout(() => this.setState({currLabel: null}), 2000)    
    }
  }

  preventSecondRate() { // Temp
    console.log((this.state.allowClear) ? 
      `You placed your rating. Your rating: ${this.state.userRating}. To reset your rating use doubleClick.`: 
      `Placing second rating is not allowed! Your rating: ${this.state.userRating}. You can't change your rating.`);
  }

  resetUserRating() { // Temp
    if (this.state.allowClear === true) {
      this.setState({userRating: null});
      console.log('Your rating was reseted.');
    }
  }

  setLabel(userRating) {
    if (!this.state.userRating) {
      this.setState({currLabel: Rating.ratesDict.get(parseFloat(userRating))});
    }
  }

  unsetLabel() {
    if (!this.state.userRating) {
      this.setState({currLabel: null});
    }
   }

   render() { 
    return ( 
      <div className={classList({container: true, notRated: !this.state.userRating})} onClick={(this.state.userRating) ? () => this.preventSecondRate() : null} onDoubleClick={() => this.resetUserRating()}>
        <div className={styles.stars}>
          {this.createStars()}
        </div>
        {this.state.currLabel && <Label className={styles.label}> {this.state.currLabel} </Label>}
      </div>
    );
  }
}
 
export default Rating;