import React from 'react';

const Label = ({className, children}) => (
  <div className={className}>
    {children}
  </div>
)

export default Label;