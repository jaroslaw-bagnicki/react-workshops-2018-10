import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Navigation.module.scss';

const Navigation = () => {
  return (
    <ul className={styles.nav}>
      <li className={styles.navItem}>
        <NavLink to="/movies" activeClassName={styles.active}>Movies</NavLink>
      </li>
      <li className={styles.navItem}>
        <NavLink to="/mybookings" activeClassName={styles.active}>My Bookings</NavLink>
      </li>
      <li className={styles.navItem}>
        <NavLink to="/redirect" activeClassName={styles.active}>Coming Soon</NavLink>
      </li>
    </ul>
  )
}

export default Navigation;