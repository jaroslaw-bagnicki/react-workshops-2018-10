import React from 'react';
import styles from './'
import Card from '../Card';

const data = [
  {
      title: 'First Man',
      releaseDate: '18.10.2018',
      desc: 'Fragment życia astronauty Neila Armstronga i jego legendarnej misji kosmicznej, dzięki której jako pierwszy człowiek stanął na Księżycu.',
      duration: 8460,
      img: 'https://i.imgur.com/0oo7XJc.jpg',
      rating: 3.68,
      userRating: null,
      soldedOut: false,
      id: 0,
      alert: {
          message: 'Wyprzedane',
          type: 'error',
      },
      shows: [
          { id: 1, time: '17:40' },
          { id: 2, time: '19:50'},
          { id: 3, time: '22:20' }
      ]
  },
  {
      title: 'Mission: Impossible - Fallout',
      desc: 'Konsekwencje zakończonej niepowodzeniem misji IMF może odczuć cały świat. Aby zapobiec katastrofie, Ethan Hunt i jego zespół muszą stanąć do wyścigu z czasem.',
      duration: 8820,
      releaseDate: '09.09.2018',
      img: 'https://i.imgur.com/rOXaXH6.jpg',
      rating: 4.85,
      userRating: 4.5,
      soldedOut: false,
      id: 1,
      alert: {
          message: 'Ostatnie miejsca',
          type: 'warning',
      },
      shows: [
          { id: 1, time: '17:40' },
          { id: 2, time: '19:50'},
          { id: 3, time: '22:20' }
      ]
  },
  {
      title: 'American Animals',
      releaseDate: '01.09.2018',
      desc: 'Wracając od kolegi, Will zauważa coś przerażającego. Pobliskie laboratorium rządowe skrywa złowrogą tajemnicę. Ogólnie jest nie za wesoło, ale wszystko kończy się dobrze i żyją długo i szczęśliwie.',
      duration: 3200,
      img: 'https://i.imgur.com/3koreob.jpg',
      rating: 3.12,
      userRating: null,
      soldedOut: true,
      id: 2,
      alert: {
          message: 'Nowość!',
          type: 'success',
      },
      shows: [
          { id: 1, time: '17:40' },
          { id: 2, time: '19:50'},
          { id: 3, time: '22:20' }
      ]
  },
];

const MoviesList = () => {
  return (
    <div className={styles.container}>
      {data.map((movie =>
          <Card {...movie} key={movie.id} shows={movie.shows} />))
      }
    </div>
  )
}

export default MoviesList;