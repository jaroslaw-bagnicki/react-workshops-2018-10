import React, {Component, Fragment} from 'react';
import styles from './Booking.module.scss';
import Button from '../Button';
import Alert from '../Alert';
import classNames from 'classnames/bind';

const classList = classNames.bind(styles);

class Booking extends Component {
  constructor(props) {
    super();
    this.shows = props.shows
    this.soldedOut = props.soldedOut
    this.state = {
      selectedShow: null,
      seatsNo: 0,
      errorMessage: null
    };
  }

  selectShow = (id) => {
    this.setState({selectedShow: id});
    this.resetErrors();
 }

  updateSeatNo = (e) => {
     this.setState({seatsNo: e.target.value});
     this.resetErrors();
  }

  buyTickets = () => {
    this.validate();
    if (this.state.errorMessage) return;
    console.log('Bilety kupione :)');
  }

  validate = () => {
    console.log(typeof this.state.seatsNo);
    if (this.state.selectedShow === null) {
      this.setState({errorMessage: 'Nie wybrałeś seansu!'});
      return;
    }
    if (this.state.seatsNo <= 0) {
      this.setState({errorMessage: 'Ilość miejsc jest niepoprawna!'});
      return;
    }
  }

  resetErrors = () => this.setState({errorMessage: null});

  renderAlert() {
    console.log(this.state.errorMessage);
    return <Alert message={this.state.errorMessage} type={'error'} fontSize='.9em' />
  }

  render() {
    return (
      <Fragment>
        <div className={styles.container}>
          <div className={styles.showsList}>
            { this.soldedOut ? 
            <Fragment>
              {this.shows.map((show => (
              <div className={classList({showButton:true, active: (show.id === this.state.selectedShow)})} key={show.id} id={show.id} onClick={(e) => this.selectShow(show.id)}>
                {show.time}
              </div>)
              ))}
            </Fragment>
              : null
            }
          </div>
          <div className={styles.inputWrapper}>
            { this.soldedOut ? 
              <Fragment>
                <label htmlFor="seatsNo">Ilość miejsc</label>
                <input id="seatsNo" name="seatsNo" type="number" value={this.state.seatsNo} onChange={(e) => this.updateSeatNo(e)} />
              </Fragment>
              : null
            }
          </div>
          <div className={styles.buttonWrapper} >
            { this.soldedOut ? 
              <Button onClick={this.buyTickets}>Kup bilet</Button>
              :
              <div className={styles.soldOut}>Wyprzedane</div>
            }
          </div>
        </div>
        { this.state.errorMessage && this.renderAlert()}
      </Fragment>
    )
  }
}

export default Booking;