import React, {Component} from 'react';
import styles from './Counter.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-regular-svg-icons';

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
    }
  }

  changeValue(val) {
    this.setState((prevState) => {
      console.log(prevState, val);
      let counter = prevState.counter + val;
      counter = (counter < 0) ? 0 : counter;
      return {
         counter
      }
    })
  }

  render() {
    return (
      <div className={styles.container}>
        <button className={[styles.button, styles.decrease].join(' ')} onClick={() => this.changeValue(-1)}>
          <FontAwesomeIcon icon={faThumbsDown} className={styles.decrease}/>
        </button>
        <span className={styles.counter}>{this.state.counter}</span>
        <button className={[styles.button, styles.increase].join(' ')} onClick={() => this.changeValue(1)}>
          <FontAwesomeIcon icon={faThumbsUp} />
        </button>
      </div>
    )
  }
}

export default Counter;