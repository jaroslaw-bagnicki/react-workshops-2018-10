import React from 'react';
import PropTypes from 'prop-types';
import styles from './Alert.module.scss';

const iconType = {
  success: 'check-circle',
  error: 'exclamation-circle',
  warning: 'exclamation-triangle',
  info: 'info-circle'
}

const Alert = ({message, children, type, fontSize = '1em'}) => {
  return (
    <div className={`${styles.container} ${styles[type]}`} style={{fontSize: fontSize}}>
      <img src={require(`./icons/${iconType[type]}.svg`)} alt="" className={styles.icon} />
      <div className={styles.message}>{message}</div>
    </div>

  )
}

Alert.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
}

export default Alert;